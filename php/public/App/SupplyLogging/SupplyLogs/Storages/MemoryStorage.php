<?php


namespace App\SupplyLogging\SupplyLogs\Storages;


use App\SupplyLogging\SupplyLogs\Storages\MemoryStorageInterface;

/**
 * записывает в память промежуточные результаты
 * @package App\SupplyLogging\SupplyLogs
 */
class MemoryStorage implements MemoryStorageInterface
{
    /** @var array */
    private $result = [];

    /**
     * суммирует количество коробок и яблок с одинаковыми остальными исходными данными
     * @param array $logsParsedWithCommas - лог, распарсенный по запятым в массив
     */
    public function add(array $logsParsedWithCommas): void
    {
        $supplier = $logsParsedWithCommas[0];
        $warehouse = $logsParsedWithCommas[1];
        $terminal = $logsParsedWithCommas[2];
        $sort = $logsParsedWithCommas[3];
        $boxes = (int)$logsParsedWithCommas[4];
        $quantity = (int)$logsParsedWithCommas[5];

        if (!isset($this->result[$supplier][$warehouse][$terminal][$sort])) {
            $this->result[$supplier][$warehouse][$terminal][$sort] = [
                'box' => 0,
                'quantity' => 0,
            ];
        }
        $this->result[$supplier][$warehouse][$terminal][$sort]['box'] += $boxes;
        $this->result[$supplier][$warehouse][$terminal][$sort]['quantity'] += $quantity;
    }

    /**
     * переписывает сумированный массив в результирующий текст, разделённый переносами строк
     * @return \Generator
     */
    public function resultIterator(): \Generator
    {
        foreach ($this->result as $supplier => $warehouses) {
            foreach ($warehouses as $warehouse => $terminals) {
                foreach ($terminals as $terminal => $sorts) {
                    foreach ($sorts as $sort => $boxesAndQuantity) {
                        yield "{$supplier},{$warehouse},{$terminal},{$sort},{$boxesAndQuantity['box']},{$boxesAndQuantity['quantity']}";
                    }
                }
            }
        }
    }
}