<?php


namespace App\SupplyLogging\SupplyLogs\Storages;

/**
 * Итерфейс для временного хранилища промежуточных вычислений
 * @package App\SupplyLogging
 */
interface MemoryStorageInterface
{
    /**
     * суммирует количество коробок и яблок с одинаковыми остальными исходными данными
     * @param array $logsParsedWithCommas - исходный лог в виде строки
     */
    public function add(array $logsParsedWithCommas);

    /**
     * переписывает сумированный массив в результирующий текст, разделённый переносами строк
     * @return \Generator
     */
    public function resultIterator(): \Generator;
}