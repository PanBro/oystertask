<?php


namespace App\SupplyLogging\SupplyLogs\Savers;

use App\SupplyLogging\SupplyLogs\Savers\Exceptions\SaverException;
use mysql_xdevapi\Exception;

/**
 * сохраняет результат в файл
 * @package App\SupplyLogging\SupplyLogs\Savers
 */
class FileSaver implements SaverInterface
{
    /** @var string */
    private $name;

    /**
     * Сохраняет результат
     * @param \Generator $data
     * @throws SaverException
     */
    public function save(\Generator $data): void
    {
        if ($this->name === null) {
            throw new SaverException('Нет имени файла.');
        }

        $resultFile = fopen($this->name, 'w');
        if ($resultFile === false) {
            throw new SaverException("Не удалось открыть файл {$this->name} для записи");
        }

        foreach ($data as $item) {
            $line = $item . PHP_EOL;
            fwrite($resultFile, $line);
        }

        fclose($resultFile);
    }

    /**
     * Задает имя результирующего лога
     * @param string $name
     * @return SaverInterface
     */
    public function setName(string $name): SaverInterface
    {
        $this->name = $name;

        return $this;
    }
}