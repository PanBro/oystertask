<?php


namespace App\SupplyLogging\SupplyLogs\Savers;

use App\SupplyLogging\SupplyLogs\Savers\Exceptions\SaverException;

/**
 * Сохраняет результат
 * Interface SaverInterface
 * @package App\SupplyLogging\SupplyLogs\Savers
 */
interface SaverInterface
{
    /**
     * Сохраняет результат
     * @param \Generator $data
     * @throws SaverException
     */
    public function save(\Generator $data): void;

    /**
     * Задает имя результирующего лога
     * @param string $name
     * @return SaverInterface
     */
    public function setName(string $name): SaverInterface;
}