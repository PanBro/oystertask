<?php


namespace App\SupplyLogging;

use App\SupplyLogging\SupplyLogs\Savers\Exceptions\SaverException;
use App\SupplyLogging\SupplyLogs\Savers\SaverInterface;
use App\SupplyLogging\SupplyLogs\Storages\MemoryStorageInterface;
use App\SupplyLogging\SupplyLogs\Storages\MemoryStorage;
use Generator;

/**
 * создует файл с ссумированным количеством коробок и яблок из исходного.
 * принимает на вход дату, на которую требуется суммировать логи.
 * если класс будет вызван без параметров - будет работать с текущей датой.
 * @package App
 */
class SupplyLogging
{
    /**
     * @var string - дата в формате "yyyy-mm-dd"
     */
    private $logFileName;

    /**
     * @var MemoryStorage - временное хранилище
     */
    private $storage;

    /** @var SaverInterface */
    private $saver;

    public function __construct(MemoryStorageInterface $storage, SaverInterface $saver, string $logFileName = null)
    {
        if ($logFileName === null) {
            $this->logFileName = date('Y-m-d');
        }
        $this->logFileName = $logFileName;

        $this->storage = $storage;
        $this->saver = $saver;
    }

    /**
     * читает исходный файл и записывает результат работы класса в другой файл
     * @return string - путь до нового файла с итоговыми значениями (сумированное количество коробок и яблок)
     */
    public function readAndWriteLogs(): string
    {
        $pathToSourceFile = $this->takePath('data');
        $sourceLogs = $this->openFileLineByLine($pathToSourceFile);

        $resultingLog = [];
        foreach ($sourceLogs as $stringLog) {
            $logsParsedWithCommas = $this->parseStringWithCommas($stringLog);
            $this->storage->add($logsParsedWithCommas, $resultingLog);
        }

        $pathToResultFile = $this->takePath('result');

        $resultIterator = $this->storage->resultIterator();
        try {
            $this->saver
                ->setName($pathToResultFile)
                ->save($resultIterator);
        } catch (SaverException $saverException) {
            return $saverException->getMessage();
        }

        return $pathToResultFile;
    }

    /**
     * открывает файл с логами построчно
     * @param string $path
     * @return Generator
     */
    private function openFileLineByLine(string $path): Generator
    {
        $openFileInArray = file($path);
        foreach ($openFileInArray as $oneLine) {
            yield $oneLine;
        }
    }

    /**
     * формирует имя файла, в котором лежат исходные логи
     * @param string $folder - имя папки, в которой будет храниться, или
     * @return string
     */
    private function takePath(string $folder): string
    {
        return "App/SupplyLogging/SupplyLogs/{$folder}/{$this->logFileName}.data";
    }

    /**
     * разбивает по запятым строку лога в массив
     * @param string $stringLog
     * @return array
     */
    private function parseStringWithCommas(string $stringLog): array
    {
        return $field = explode(',', $stringLog);
    }
}