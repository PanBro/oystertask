<?php


namespace App\WordCase;

/**
 * Подбирает подходящий падеж для количества чего-либо
 * @package App
 */
class WordCase
{
    /**
     * @param int $number - исходное число
     * @param string $nominative - описание в именительном падеже
     * @param string $genitive - описание в родительном падеже
     * @param string $genitivePlural - описание в родительном падеже множественном числе
     * @return string
     */
    public function GetIntWord(int $number, string $nominative, string $genitive, string $genitivePlural): string
    {
        $dozens = $number % 100;
        if ($dozens >= 11 && $dozens <= 14) {
            return "$number $genitivePlural";
        }

        $units = $dozens % 10;
        if ($units == 1) {
            return "$number $nominative";
        }

        if ($units >= 2 && $units <= 4) {
            return "$number $genitive";
        }

        return "$number $genitivePlural";
    }
}