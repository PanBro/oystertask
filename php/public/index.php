<?php declare(strict_types=1);

use App\SupplyLogging\SupplyLogs\Savers\FileSaver;
use App\SupplyLogging\SupplyLogs\Storages\MemoryStorage;
use App\WordCase\WordCase;
use App\SupplyLogging\SupplyLogging;


require_once 'vendor/autoload.php';
define('ROOT', dirname(__FILE__));

////РАССКОМЕНТИРУЙТЕ ДЛЯ ОТОБРАЖЕНИЯ РЕЗУЛЬТАТОВ ПЕРВОГО ЗАДАНИЯ
////результаты первого задания (число + обозначение в правильном падеже)
//$caseWithWords = new WordCase();
//for ($number = 0; $number < 30; $number++) {
//    var_dump($caseWithWords->GetIntWord($number, 'день', 'дня', 'дней'));
//}

//РАССКОМЕНТИРУЙТЕ ДЛЯ ОТОБРАЖЕНИЯ РЕЗУЛЬТАТОВ ВТОРОГО ЗАДАНИЯ
//Результаты выполнения второго задания: созаёт файл с обработанными логами (коробки и количество ссумированы)
//Принимает параметром имя в формате "yyyy-mm-dd". Если вызывать без параметров - будет искать файл с текущей датой в названии
$storage = new MemoryStorage();
$saver = new FileSaver();
$supplyLogging = new SupplyLogging($storage, $saver, '2018-12-01');
var_dump($supplyLogging->readAndWriteLogs());